// CRUD OEPRATIONS

// insert Documents (CREATE)

/*
	Syntax:
		Insert One Document
			db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB",
			});

		Insert Many Documents
			db.collectionName.InsertMany([
			{
				"fieldA": "valueA",
				"fieldB": "valueB",
			},

			{
				"fieldA": "valueA",
				"fieldB": "valueB",
			}

			])
*/

db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"email": "janedoe@mail.com",
	"company": "none"

})

db.users.insertMany([
	{

		"firstName": "Stephen",
		"lastName": "Hawking",
		"age": 76,
		"email": "stephenhawking@mail.com",
		"company": "none",
	},

	{
		"firstName": "Neil",
		"lastName": "Armstrong",
		"age": 82,
		"email": "neilarmstrong@mail.com",
		"company": "none"
	}
])

//Mini Activity
	/*
		1. Make a new collection with the name "courses"
		2. Insert the following fields and values

			name: Javascript 101
			price: 5000
			description: Introduction to Javascript
			isActive: true

			name: HTML 101
			price: 2000
			description: Introduction to HTML
			isActive: true

			name: CSS 101
			price: 2500
			description: Introduction to CSS
			isActive: true
	*/

	// Find Documents (Read/Retrieve)

	db.users.find(); // This will retreive all the documents in the collection

	db.users.find({
		"firstName": "Jane"
	});

	db.users.find({ 
		"firstName": "Neil",
		"age": 82
	});

	db.users.findOne({ //Returns the first document in our collection

	});

	db.users.findOne({
		"firstName": "Stephen"
	})

	// Update Documents

	db.users.insertOne(
		{
		"firstName": "test",
		"lastName": "testt",
		"age": 0,
		"email": "test@mail.com",
		"company": "none"
		})

// Updating one Document

db.users.updateOne(
{
	"firstName": "Bill"
},

{
	$set:{
		"firstName": "Bill",
		"lastName": "Gates",
		"age": 65,
		"email": "billgates@mail.com",
		"department": "Operations",
		"status": "active"
	}
}
)

// Removing a field
db.users.updateOne(
	{
		"firstName": "Bill"

	},
	{
		$unset:{
			"status": "active"
		}
	}

)

// Update Multiple Documents

db.users.updateMany(
	{
		"company": "none",
	},
	{
		$set:{
			"company": "HR"
		}
	}
)

db.users.updateOne(
{}
	{
		$set:{
			"company": "operations"
		}
	}
)

db.users.updateMany(
{}
	{
		$set:{
			"company": "comp"
		}
	}
)

// Deleting Documents (Delete)

db.users.insertOne({
	"firstName": "Test"

});

db.users.deleteOne({
	"firstName": "Test"
})

db.users.deleteMany({
	"company": "comp"
})

db.courses.deleteMany({
	
})