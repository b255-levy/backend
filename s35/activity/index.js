const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 4000;

mongoose.connect("mongodb+srv://patrick-255:admin123@zuitt-bootcamp.a8lpb2y.mongodb.net/?retryWrites=true&w=majority", {
  useNewUrlParser : true,
  useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  }
});

const User = mongoose.model("User", userSchema);

app.use(express.json());

const createUser = (req, res) => {
  const { username, password } = req.body;

  User.findOne({ username })
    .then(user => {
      if (user) {
        return res.status(400).send('Duplicate username found');
      }

      if (!username || !password) {
        return res.status(400).send('Both username and password must be provided');
      }

      const newUser = new User({ username, password });

      newUser.save()
        .then(() => {
          res.status(201).send('New user registered');
        })
        .catch(err => {
          console.error(err);
          res.status(500).send('Error saving user');
        });
    })
    .catch(err => {
      console.error(err);
      res.status(500).send('Error finding user');
    });
};


app.post("/signup", createUser);

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});
