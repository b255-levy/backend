console.log("meow")

// An array in programming is simply a list of data. Let's write the example earlier

let studentnumberA = "2020-1923";
let studentnumberB = "2020-1924";
let studentnumberC = "2020-1925";
let studentnumberD = "2020-1926";
let studentnumberE = "2020-1927";

let studentNumbers = ['2020-1923', "2020-1924", "2020-1925", "2020-1926", "2020-1927"];

// [SECTION] Arrays

/*
	-Arrays are used to store multiple related values in a single variable
	-They are declared using square brackets "[]". These square brackets are also known as "Array Literals"
	-Commonly used to store numerous amounts of data to manipulate in order to perform a number of tasks.
*/

// Common examples for arrays
let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Acer', 'Lenovo', 'Neo','Redfox','Gateway','Toshiba','Fujitsu',];

// Possible use of an array but is not recommended
let mixedArr = [12, 'Asus', null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

// Alertnative way to write arrays

let myTask = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass',
];

// Creating an array with values from variables

let city1 = 'Tokyo'
let city2 = 'Manila'
let city3 = 'Jakarta'

let cities = [city1, city2, city3]

console.log(myTask);
console.log(cities);

// [SECTION] lenght property

// The .length property allows us to get and set the total number of items in an array

console.log(myTask.length);
console.log(cities.length);

let blankArr = [1];
console.log(blankArr.length);

// length peropty can also be used with strings. Some array methods and properties can also be used with strings.

let fullname = 'Jamie Noble';
console.log(fullname.length);

// Length property can also set the total number of items in an array, meaning we can actually delete the last item in the array.

myTask.length = myTask.length-1;
console.log(myTask.length);
console.log(myTask);

// To delete a specific item in an aray we can employ array methods(which will be shown in the next session)

// Another example of using decrement

cities.length--;
console.log(cities);

fullname.length = fullname.length-1;
console.log(fullname.length)
fullname.length--;
console.log(fullname);

// If you can shortern the arrat by setting the length property, you can also lengthen it by adding a number into the length propery.

let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++
console.log(theBeatles)

// [SECTION] Reading from arrays

/*
	-Accessing array elements is one of the more common tasks that we do with an array
	-This can be done through the use of array indexes
	-Each element in an array is associated with its own index/number
	-In Javascript, the first element is associated with the number 0 and increasing this number by 1 for every proceeding element
	-The reason an array starts with 0 is due to how the language is designed
	-Array indexes actually refer to an address or location in the devices memory and how the information is stored
	-Example array location in memory
		Array[0] = 0x7ffe947bad0
		Array[0] = 0x7ffe947bad4
		Array[0] = 0x7ffe947bad8
*/


console.log(grades[0]);
console.log(computerBrands[0]);
// Accessing an array element that does not exist will return "undefined"

console.log(grades[20])

let lakersLegends = ["Kobe", "Shaq", "LeBron", "Magic", "Kareem"];
console.log(lakersLegends[1])
console.log(lakersLegends[3])

// You can save/store array items in another variable
let currentLaker = lakersLegends[2];
console.log(currentLaker);

// You can also reassign array values using the item's idices

console.log("Array before reassignment");
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";
console.log("Array after reassignment");
console.log(lakersLegends)

// Accessing the last element of an array
// Since the first element of an array starts with 0, subtracting 1 to the length of an array will offset the value by one allowing us to get the last index

let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"]

let lastElementIndex = bullsLegends.length-1;

console.log(bullsLegends[lastElementIndex]);

// You can also add it directly
console.log(bullsLegends[bullsLegends.length-1])

// Adding items into the array

let newArr = [];
console.log(newArr[0]);
newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "Tifa Lockhart";
console.log(newArr);

// You can also add items at the end of the array.
// newArr[newArr.lenght-1] = "Aerith Gainsborough"
newArr[newArr.length] = "Barret Wallace"
console.log(newArr);

// Looping over an array
// You can use a "For loop" to iterate over all the items in an array
// Set the counter as the index and set a condition that as long as the current index iterated is less than the length of the array

for(let index = 0; index<newArr.length; index++){
	console.log(newArr[index]);
}

let numArr = [5,12,30,46,40];

for(let index = 0; index < numArr.length; index++){
	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " Is divisible by 5");
	} else {
		console.log(numArr[index] + " Is not divisible by 5");
	}

}

// [SECTION] MultiDimensional Array

/*
	-Multidimensional arrays are useful for storing complex data structures
	-A practical application of this is to help visualzie/create real world objects
	-Though useful in a number of cases, creating complex array structure is not always recommended
*/

let chessBoard= [
	['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'h1'],
	['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'h2'],
	['a3', 'b2', 'c3', 'd3', 'e3', 'f3', 'h3'],
	['a4', 'b3', 'c4', 'd4', 'e4', 'f4', 'h4'],
	['a5', 'b4', 'c5', 'd5', 'e5', 'f5', 'h5'],
	['a6', 'b5', 'c6', 'd6', 'e6', 'f6', 'h6'],
	['a7', 'b6', 'c7', 'd7', 'e7', 'f7', 'h7'],
	['a8', 'b7', 'c8', 'd8', 'e8', 'f8', 'h8']
]

console.log(chessBoard);

// Accessing elements of a multidimensional array
console.log(chessBoard[1][4])

console.log("Pawn moves to: " + chessBoard[1][5]);