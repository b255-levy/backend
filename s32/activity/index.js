const http = require('http');

const port = 4000;

const server = http.createServer((request, response) => {
  if (request.url === '/') {
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end('Welcome to booking system');
  } else if (request.url === '/profile') {
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end('Welcome to your profile');

  } else if (request.url === '/courses') {
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end('Here\'s our courses available');

  } else if (request.url === '/addCourse') {
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end('Add a course to our resources');

  } else if (request.url === '/archiveCourse') {
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end('Archive courses to our resources');

  } else if (request.url === '/updateCourse') {
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end('Update course to our resources');

  } else {
    response.writeHead(404, {'Content-Type': 'text/plain'});
    response.end('404 Not Found\n');
  }
});

server.listen(4000);

  console.log(`Server running at http://localhost:4000/`);