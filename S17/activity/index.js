/*
	//Note: strictly follow the variable names and function names.

	1. Create a function named printUserInfo() which is able to display a user's to fullname, age, location and other information.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
*/
	//first function here:
	function printUserInfo() {
	let fullName = "Patrick Levy";
	let age = "26";
	let location = "Makati City";
	let petName = "Pupper";
	console.log("Hello, I am " + fullName);
	console.log("I am " + age);
	console.log("I live in " + location);
	console.log("My pet's name is " + petName);

};
printUserInfo();

/*
	2. Create a function named printFiveBands which is able to display 5 bands/musical artists.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	//second function here:

	function printFiveBands(){
		let band1 = "Taking Back Sunday";
		let band2 = "Mayday Parade";
		let band3 = "Fallout Boy";
		let band4 = "Greenday";
		let band5 = "Linkin Park";
		console.log(band1);
		console.log(band2);
		console.log(band3);
		console.log(band4);
		console.log(band5);

	};

	printFiveBands();



/*
	3. Create a function named printFiveMovies which is able to display the name of 5 movies.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	
	//third function here:
	function printFiveMovies(){
		let movie1 = "Wreck-it Ralph";
		let movie2 = "Fight Club";
		let movie3 = "Shawshank Redemption";
		let movie4 = "Gladiator";
		let movie5 = "Lion King";
		console.log(movie1);
		console.log(movie2);
		console.log(movie3);
		console.log(movie4);
		console.log(movie5);

	};

printFiveMovies();



/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
		-check your spelling

		-invoke the function to display information similar to the expected output in the console.
*/






function printFriends(){
	let friend1 = "Eugene"; 
	let friend2 = "Dennis"; 
	let friend3 = "Vincent";

	console.log("These are my friends:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);








//Do not modify
//For exporting to test.js
try{
	module.exports = {
		printUserInfo,
		printFiveBands,
		printFiveMovies,
		printFriends
	}
} catch(err){

}
