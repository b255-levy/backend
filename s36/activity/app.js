
const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute");

const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://patrick-255:admin123@zuitt-bootcamp.a8lpb2y.mongodb.net/?retryWrites=true&w=majority",{
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log('Connected to MongoDB!');
  })
  .catch((err) => {
    console.error(err);
  });

app.use("/tasks", taskRoute);


if(require.main === module){
	app.listen(port, () => console.log(`Server Running at ${port}`))
}


const { EventEmitter } = require("events");
const Bus = new EventEmitter();
Bus.setMaxListeners(20); // Set maximum number of listeners to 20

module.exports = app;
