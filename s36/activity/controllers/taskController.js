const Task = require("../models/task");

module.exports.getAllTasks = () => {
  return Task.find({});
};

module.exports.createTask = (requestBody) => {
  let newTask = new Task({
    name: requestBody.name
  });

  return newTask.save();
};

module.exports.deleteTask = (taskId) => {
  return Task.findByIdAndRemove(taskId);
};

module.exports.updateTask = (taskId, newContent) => {
  return Task.findById(taskId).then((result) => {
    result.name = newContent.name;
    return result.save();
  });
};

module.exports.getTask = (req, res) => {
  const taskId = req.params.id;
  Task.findById(taskId)
    .then((task) => {
      if (!task) {
        return res.status(404).json({ message: "Task not found" });
      }
      return res.json(task);
    })
    .catch((err) => {
      console.error(err);
      return res.status(500).json({ message: "Server Error" });
    });
};

module.exports.completeTask = (req, res) => {
  const taskId = req.params.id;
  Task.findById(taskId)
    .then((task) => {
      if (!task) {
        return res.status(404).json({ message: "Task not found" });
      }
      task.status = "completed";
      return task.save();
    })
    .then((updatedTask) => {
      return res.json(updatedTask);
    })
    .catch((err) => {
      console.error(err);
      return res.status(500).json({ message: "Server Error" });
    });
};
