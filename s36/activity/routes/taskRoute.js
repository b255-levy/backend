const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController");

router.get("/", (req, res) => {
  taskController.getAllTasks()
    .then(result => {
      return res.json(result);
    });
});

router.post("/", (req, res) => {
  taskController.createTask(req.body)
    .then(result => {
      return res.json(result);
    });
});

router.delete("/:id", (req, res) => {
  taskController.deleteTask(req.params.id)
    .then(result => {
      if (result) {
        return res.json({ message: "Task deleted successfully" });
      } else {
        return res.status(404).json({ message: "Task not found" });
      }
    });
});

router.put("/:id", (req, res) => {
  taskController.updateTask(req.params.id, req.body)
    .then(result => {
      if (result) {
        return res.json(result);
      } else {
        return res.status(404).json({ message: "Task not found" });
      }
    });
});

router.get('/:id', (req, res) => {
  taskController.getTask(req, res);
});

router.put('/:id/complete', (req, res) => {
  taskController.completeTask(req, res);
});

module.exports = router;
