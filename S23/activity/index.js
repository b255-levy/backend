console.log("Hello World");

//Follow the property names and spelling given in the google slide instructions.

// Create an object called trainer using object literals

let trainer = {
	name: 'Ash',
	age: 12,
	pokemon: ["Pikachu", "Bulbasaur", "Charmander", "Squirtle" ],
	friends: {
    "Kanto": ["Misty", "Brock"],
    "Hoenn": ["May", "Max"]
    },

	talk: function(){
		console.log("Pikachu! I choose you!");
	}
}
console.log("Result from calling object trainer");
console.log(trainer);
console.log("Result from dot notation");
console.log(trainer.name);
console.log("Result from calling square bracket notation");
console.log(trainer['pokemon'])
console.log("Result of talk method")
trainer.talk()


// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon

function pokemon(name, level,) {
	this.name = name
	this.level = level
	this.health = level*2
	this.attack = level*2

	this.tackle = function(target) {
    target.health -= this.attack;
    console.log(`${this.name} tackled ${target.name} for ${this.attack} damage!`);
    if (target.health <= 0) {
    	target.faint();
    }

}
	this.faint = function (){
		console.log(`${this.name} has fainted!`);
	}}

let Pikachu = new pokemon('Pikachu', 5)

console.log(Pikachu);
// Create/instantiate a new pokemon
let Squirtle = new pokemon('Squirtle', 7)

console.log(Squirtle);

// Create/instantiate a new pokemon
let Charmander = new pokemon('Charmander', 11)

console.log(Charmander);

// Create/instantiate a new pokemon
let Bulbasaur = new pokemon('Bulbasaur', 9)

console.log(Bulbasaur);

// Invoke the tackle method and target a different object

Pikachu.tackle(Bulbasaur);
console.log(Bulbasaur);
Bulbasaur.tackle(Pikachu);
console.log(Pikachu)

// Invoke the tackle method and target a different object








//Do not modify
//For exporting to test.js
try{
	module.exports = {
		trainer,
		Pokemon 
	}
} catch(err) {

}
