

//3.
fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(data => console.log(data))

//4.
fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(data => {
    const titles = data.map(item => item.title);
    console.log(titles);
    })

//5.
  fetch('https://jsonplaceholder.typicode.com/todos/1')
    .then(response => response.json())
    .then(data => console.log(data))

// 6.
fetch('https://jsonplaceholder.typicode.com/todos/1')
  .then(response => response.json())
  .then(data => {
    const { title, completed } = data;
    console.log(`Title: ${title}, Status: ${completed ? 'Completed' : 'Incomplete'}`);
  })
      


// 7.
fetch('https://jsonplaceholder.typicode.com/todos', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    title: 'Created to do list item',
    completed: false,
    userId: 1
  })
})
  .then(response => response.json())
  .then(data => console.log(data))


//8. 

fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    id: 1,
    title: 'Updated to do list Item',
    description: 'To update the my to do list with a different data structure',
    status: 'Pending',
    dateCompleted: 'Pending',
    userId: 1
  })
})
  .then(response => response.json())
  .then(data => console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos/1', {

  method: 'PATCH',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    status: "Complete",
    dateCompleted: "07/09/21"
    })
  })
    .then(response => response.json())
    .then(data => console.log(data))
    

fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'DELETE'
    })
   .then(response => response.json())
   .then(data => console.log(data))
