const express = require ("express")

const app = express()

const port = 4000

app.use(express.json());

app.use(express.urlencoded({extended:true}));

app.get('/home', (req, res) => {
  res.send('Welcome to the home page');
});

let users = [{ username: "johndoe", password: "johndoe1234" }];

app.get('/user', (req, res) => {
  res.send(users);
});

console.log(users)

app.delete('/deleteuser', (req, res) => {
  let username = req.body.username;
  let message;

  if (users.length > 0) {
    for (let i = 0; i < users.length; i++) {
      if (users[i].username === username) {
        users.splice(i, 1);
        message = `User ${username} has been deleted`;
        break;
      }
    }

    if (!message) {
      message = 'User does not exist';
    }
  } else {
    message = 'No users found';
  }

  res.send(JSON.stringify({ message }));

});



if(require.main === module){
  app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app;
