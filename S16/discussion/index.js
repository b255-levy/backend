console.log("Hello World");

// Arithmetic Operators

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of divison operator: " + quotient);

let remainder = y % x;
console.log("Result of modulo operator: " + remainder);

// Assignment Operators

	// Basic assignment operator (=)
	let assignmentNumber = 8;

	// Addition assignment operator (+=)
	// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.

	assignmentNumber = assignmentNumber + 2;
	console.log("Result of addition assignment operator: " + assignmentNumber)

	//  Shorthand
	assignmentNumber += 2;
	console.log("Result of addition assignment operator: " + assignmentNumber);

	assignmentNumber -= 2;
	console.log("Result of subtraction assignment operator: " + assignmentNumber);

	assignmentNumber *= 2;
	console.log("Result of multiplication assignment operator: " + assignmentNumber);

	assignmentNumber /= 2;
	console.log("Result of divison assignment operator: " + assignmentNumber);

// Multiple Operator and Parentheses

	/*
		- When multiple operators are applied in a single statement, it follows the PEMDAS.
	*/

	let mdas = 1 + 3 - 3 * 4 / 5;
	console.log("Result of mdas operation: " + mdas);

	let pemdas = 1 + (2 - 3) * (4/5);
	console.log("Result of pemdas operation: " + pemdas);

// Increment and decrement

let z =1;
let increment = ++z
console.log("Result of increment " + increment);

let decrement = --z;
console.log("Result of decrement " + decrement);

// Type coercion

/*
	-Type coercion is the automatic or implicit conversion of values from one data type to another 
	-This happens when operations are performed on different data types that would normally not be possible and yield irregular results.
	-Values are automatically converted from one data type to another in order to resolve operations
*/

let numA = '10';
let numB = 12;

// Adding/concatenating a string and a number will result in a string

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numE = true + 1;
console.log(numE);

let numF = false + 1;
console.log(numF);

// Comparison Operators

let juan = "juan";

// equality operator(==)
/*
	-checks whether the operands are equal/have the same content
	-returns a boolean value
*/

console.log(1==1);
console.log(1==2);
console.log(1=='1');
console.log(0==false);

// compares two strings that are teh same
console.log('juan' == 'juan');
// compares a string with the variable "juan" declared above
console.log('juan' == juan);

// inequality operator
/*
	-checks whether the operands are not equal/have different content
*/

console.log("START OF INEQUALITY OPERATORS: ")
console.log(1 !=1);
console.log(1 !=2);
console.log(1 !='1');
console.log(1 != false);
console.log('juan' !='juan');
console.log('juan' != juan);

// Strict Equality Operator
/*
	-checks whether the operands are equal/have the same content
	-also COMPARES the data types of 2 values
	-JavaScript is loosely typed language meaning that values of diffferent data types can be stored in variables
	-This sometimes can cause problems within our code
*/

console.log("START OF STRICT EQUALITY OPERATOR");
console.log(1===1);
console.log(1===2);
console.log(1==='1');
console.log(0=== false);
console.log('juan' === 'juan');
console.log('juan' === juan);

// Strict inequality operator

console.log("START OF STRICT INEQUALITY OPERATOR");
console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log('juan' !== 'juan');
console.log('juan' !== juan);

// Relational Operators

	// Some comparison operators check whether one value is greater or less than to the other value

	let a = 50;
	let b = 65;

	// GT or Greater Than operator (>)
	let isGreaterThan = a > b;
	// LT or Less Than operator (<)
	let isLessThan = a < b;
	// GTE or Greater Than or Equal to (>=)
	let isGTorEqual = a >= b;
	// LTE or Less Than or equal to (<=)
	let isLTorEqual = a <= b;

	console.log("START OF RELATIONAL OPERATOR");
	console.log(isGreaterThan);
	console.log(isLessThan);
	console.log(isGTorEqual);
	console.log(isLTorEqual);

	// Logical Operators

	let isLegalAge = true;
	let isRegistered = false;
	// Logical and Operator (&& - Double Ampersand)
	// Returns true if all operands are true
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("Result of Logical AND operator: " + allRequirementsMet);

	// Logical OR operator (|| - Double Pipe) 
	// Returns true if one of the operands are True
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of Logical OR operator: " + someRequirementsMet);

	// Logical NOT operator (! - Exclamation Point) 
	// Returns the opposite value

	let someRequirementsNotMet = !isRegistered;
	console.log("Result of logical NOT operator: " + someRequirementsNotMet);