/*
	
//Note: strictly follow the variable names and function names from the instructions.

*/


//Do not modify
//For exporting to test.js


function addNum(num1, num2) {
	sum = num1 + num2;
	console.log("Displayed sum of " + num1 + " and " + num2);
	console.log(sum);
}

addNum(5,15);

function subNum(num1, num2) {
	difference = num1 - num2;
	console.log("Displayed difference of " + num1 + " and " + num2);
	console.log(difference);
};

subNum(20,5);

function multiplyNum(num1, num2) {
	console.log("The product of " + num1 + " and " + num2 + ":");
	return num1 * num2;

};

let product = multiplyNum(50,10);
console.log(product);

function divideNum(num1, num2) {
	console.log("The quotient of " + num1 + " and " + num2 + ":");
	return num1 / num2;

};

let quotient = divideNum(50,10);
console.log(quotient);


function getCircleArea(radius){
	console.log("The result of getting the area of a circle with " + radius + " radius:")
	return Math.PI * radius ** 2 ;
};

let circleArea = getCircleArea(15);
console.log(circleArea);

function getAverage(num1, num2, num3, num4,){
	console.log("The average of " + num1 + "," + num2 + "," + num3 + " and " + num4 + ":");
	return (num1 + num2 + num3 + num4)/4
};

let averageVar = getAverage(20,40,60,80);
console.log(averageVar)


function checkIfPassed(num1, num2) {
	console.log("Is " + num1 + "/" + num2 + " a passing score?");
	return num1 / num2 * 100 >= 75;
};

let isPassed = checkIfPassed(38,50);
console.log(isPassed);





try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}
