console.log("Hello") 

// Functions
	// Parameters and Arguments
	// Functions in Javascript are lines/blocks of codes that tell our device/application to perform certain task when they are called
	// Functions are mostly created to create complicated tasks to run several lines of code in succession
	// They are also used to prevent repeating lines/blocks of code that perform the same task/function

	function printInput() {
		let nickname = prompt("Enter your nickname: ");
		console.log("Hi, " + nickname)
	};

	// printInput();

	// For other cases, functions can also process data directly passed into it instead of relying on global variables and prompts()

	function printName(name){
		console.log("My name is " + name);
	};

	printName("Juana");
	printName("Juan");

	// You can directly pass data into the function. The function can then call/use that data which is referred as "name"

	// "name" is a parameter
	// a "parameter" acts as a named variable/container that exists only inside of a function
	// It is used to store information that is provided to a function when it is called/invoked

	// Variables can also be passed as an arugment

	let sampleVariable = "Yui";
	printName(sampleVariable);

	// Function arguments cannot be used by a function if there are no parameters provided within the functions

	function checkDivisibilityBy8(num) {
		let remainder = num % 8;
		console.log("The remainder of " + num + " divided by 8 is " + remainder);
		let isDivisibilityBy8 = remainder === 0;
		console.log("Is " + num + " Divisibility by 8?");
		console.log(isDivisibilityBy8);
	};

	checkDivisibilityBy8(64);
	checkDivisibilityBy8(25);

	// Function as Arguments

	// Function parameters can also accept other functions as arguments
	// Some complex functions use other functions use other functions as arguments to perform more complicated results

	function argumentFunction() {
		console.log("This function was passed as an argument before the message was printed")
	};

	function invokeFunction(argumentFunction){
		argumentFunction();
	};


	// Adding and removing parantheses "()" impacts the output of JS heavily
	// When a function is used with parantheses "()", it denotes invoking/calling a function
	// A function used without parentheses is normally associated with using the function as an argument to another function

	invokeFunction(argumentFunction);

	// finding information about a function in the console.log()
	console.log(argumentFunction);

	// Using multiple parameters
	// Multiple "arguments" will correspond to numbers of "parameters" declared in a function of succeeding order.

	function createFullName(firstName, middleName, lastName) {
		console.log(firstName + ' ' + middleName + ' ' + lastName);
	};

	createFullName('Juan', "Dela", 'Cruz');

	// "Juan" will be stored in the parameter "firstName"
	// "Dela" will be stored in the parameter "middleName"
	// "Cruz" will be stored in the parameter "lastName"

	// In Javascript, providing more/less arguments than the expected parameters will not return an error.

	// Providing less arguments than expected parameters will automtically assign an undefined value to the parameter.

	createFullName('Juan', 'Dela');
	createFullName('Juan', 'Dela', 'Cruz', 'Hello');

	// Using variables as arguments

	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName, middleName, lastName);

	// Parameter names are just names to refer to the argument. Even if we change the name of the parameters, the arguments will be received in the same order it was passed.

	// The return statement.
	// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function

	function returnFullName(firstName, middleName, lastName,){
		return firstName + ' ' + middleName + ' ' + lastName
		console.log("This message will not be printed");
	};

	// In our example, the "returnFullName" function was invoked/called in the same line as declaring a variable
	// Whatever value is returned from the "returnFullName" function will be stored in the "completeName" variable

	let completeName = returnFullName("Jeffrey", "Amazon", "Bezos");
	console.log(completeName);

	// This way, a function is able to return a value we can further use/manipulate in our program instead of only printing/displaying it in the console

	// Mini-Activity

	// Create a functiojn that will add two numbers together
	// 1. You must use parameters and arguments



	function addition(num1, num2){
		sum = num1 + num2
		console.log(sum);
		
	}

	addition(1,2);

	// Notice that the console.log() after the return is no longer printed in the console. That is because ideally any line/block of code that comes after the return statement is ignored because it ends the function execution

	// In this example, console.log() will print the returned value of the returnFullName() function.
	console.log(returnFullName(firstName, middleName, lastName,));

	// You can also create a variable inside the fucntion to contain the result and return that variable instead

	function returnAddress (city, country){
		let fullAddress = city + ", " + country;
		return fullAddress;
	};

	let myAddress = returnAddress("Cebu City", "Cebu");
	console.log(myAddress);


	// On the other hand, when a function only has console.log() to display its results it will return undefined instead.

	function printPlayerInfo (username, level, job) {
		console.log("Username: " + username);
		console.log("Level: " + level);
		console.log("Job: " + job);
	}

	let user1 = printPlayerInfo("Knight_white", 95, "Paladin");
	console.log(user1);

	// Returns undefined because printPlayerInfo returns nothing. It only console.logs the details

	// You cannot save any value from printPlayerInfo because it does not return anything