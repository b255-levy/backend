// [SECTION] Syntax, Statements and Comments
// Statements in programming are instructions that we tell the computer to perform
// JS Statements usually end with the semicolon(;)
console.log("Hello World!");

// Comments are parts of the code that gets ignored by the language
// Comments are meant to describe the written code

/*
There are two types of comments
	1. The single line comment denoted by two slashes
	2. The multi-line comment denoted by a slash and an asterisk
*/

// [SECTION] Variables

// It is used to contain data
// Any information that is used by an application is stored in what we call a "memory"
// When we create variables, certain portions of a devices memory is given a "name" that we call "variable"

// Declaring Variables
// Declaring variables - tells our device that a variable name is created and ready to store data
// Declaring a variable without assigning a value will automatically give it the value of "undefined"

let myVariable;
console.log(myVariable);
// Console.log() is useful for printing out values of variables or certain results of code into the console.

let hello;
console.log(hello);
// Variables must be declared first before they are used
// Using variables before they're declared will return an error

/*
	Guides in writing variables:
	1.Use the 'let' keyword followed by the variable name of your chosing and use the assignment operator(=) 
	to assign a value
	2.Variable names should start with a lowercase character, use camelCase for multiple words
	3. For constant variables, use the 'const' keyword
	4. Variable names should be indicative (or descriptive) of the value stored to avoid confusion

*/

// Declaring and initializing variables
// initializing variables - the instance when a variable is given it's initial/starting value

let productName = 'desktop computer';
console.log(productName);

let productPrice = 18999;
console.log(productPrice)

// In the context of certain applications, some variables/information are constant and should not be changed
// In this example, the interest rate for a loan savings account or a mortgage must not be changed due to real world concerns

const interest = 3.539;

// Reassigning variable values
// Reassigning a variable means changing its initial or previous value into another value

productName = 'Laptop';
console.log(productName);

// Reassigning variables vs initializing variables
// Declare a variable first
let supplier;
// Initialization is done after the variable has been decalred
// This is considered as initialization because it is the first time that a value has been assigned to a variable
supplier = "John Smith Tradings";
console.log(supplier);

// This is considered a reassignment because its iniital value was already declared
supplier = "Zuitt store";
console.log(supplier);

/*
	const pi;
	pi = 3.14;
	console.log(pi);
*/

// Var vs let/const

	// var - is also used in declaring variables. but var is an ECMAScript (ES1) feature (Javascript 1997) 
	// let/const was introduced as a new feature in ES6(2015)

	// There are issues associated with variables declared with var, regarding hoisting
	// hoisting is Javascripts default behaviour of moving declartions to the top
	// In terms of variables and constants, keyword var is hoisted and let and const does not allow hoisting

	// For example:
	a = 5;
	console.log(a);
	var a;

	// In the above example, the variable a is used before declaring it. and the program works and displays the output of 5

	// let/const local/global score
	// scope essentially means where these variables are available for use
	// let and const are block scoped
	// a block is a chunk of code bounded by {} a block lives in curly braces. Anything within curly braces is a block.

	// Multiple variable declarations
	// Multiple variables may be declared in one line
	// Though it is quicker without having to retype the "let" key word it is still best practice to use multiple "let/const" keywords

	let productCode = 'DC017', productBrand = 'Dell';
	console.log(productCode, productBrand);

	// [SECTION] Data Types

	// Strings
	// Strings are a series of characters that create a word, a phrase, a sentence or anything related to creating text
	// Strings in Javascript can be written using either single ('') or double ("") quote

	let country = 'Philippines';
	let province = 'Metro Manila';

	// Concatenating strings
	// Multiple string values can be combined to create a string value using the "+" symbol
	let fullAddress = province + ',' + country;
	console.log(fullAddress);

	let greeting = 'I live in the ' +country;
	console.log(greeting)

// The escape character (\) in strings in combinations with other characters and produce different effects
// the "\n" refers to creating a new line in between text
	let mailAddress = 'Metro Manila\n\nPhilippines';
	console.log(mailAddress);

	let message = "John's employees went home early";
	console.log(message);

	message = 'John\'s employees went home early';
	console.log(message);

	// Numbers
	// Integers/Wholenunbers
	let headCount = 26;
	console.log(headCount);

	// Decimal Numbers/Fractions
	let grade = 98.7;
	console.log(grade);

	// Exponential Notation
	let planetDistance =2e10;
	console.log(planetDistance);

	// Combining text and strings
	console.log("John's grade last quarter is " + grade);

	// Boolean
	// Boolean values are normally used to store values relating to certain things
	// This will be usedful in further discussions about creating logic to create our applications respond to certain scenarios

	let isMarried = false;
	let inGoodConduct = true;
	console.log("isMarried: " + isMarried);
	console.log("inGoodConduct " + inGoodConduct);

	// Arrays
	// Arrays are a special kind of data types that's used to store multiple values
	// Arrays can store different data types but is normally used to store similiar data types 

	let grades = [98.7, 92.1, 90.2, 94.6];
	console.log(grades)

	// Different data types 

	let details = ["John", "Smith", 32, true]
	console.log(details);

	// Objects are another special kind of data type that's used to mimics real world objects/items
	// They're used to create complex data that contains pieces of information that are relevant to each other
	// Every individual piece of information is called property of the object

	let person = {
		fullName: 'Juan Dela Cruz',
		age: 35,
		isMarried: false,
		contact: ["+63917 123 4567", "8123 4567"],
		address: {
			houseNumber: '345',
			city: 'Manila'
		}
	}
	console.log(person);


	// They're also useful for creating abstract objects

	let myGrades = {
		firstGrading:98.7,
		secondGrading:92.1,
		thirdGrading:90.2,
		fourtGrading:94.6,
	}

	console.log(myGrades);

	// typeof operator is used to determine the type of data or the value of a variable.
	console.log(typeof myGrades);

	const anime = ['one piece', 'one punch man', 'attack on titan'];
	console.log(anime[0]);

	anime[0] = ['kimetsu no yaiba'];
	console.log(anime);

	// Null
	// It is used to intentionally express the absence of a value in a variable declaration/initialization
	let spouse = null;
	// Using null compared to a 0 value is much better for readiablity purposes
	// Null is also considered to be a data type of its own compared to 0 which is a data type of number

	// undefined
	// Represents the state of a variable that has been declared without an assigned value

	let fullName;
	console.log(fullName);

	// Undefined vs null
	// One clear different between undefined and null is that for undefined, a variable was created but was not provided a value
	// Null means that a variable was created and was assigned a value that does not hold any amount